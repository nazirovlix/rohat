<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<section class="top_slidero">
	            <button class="scroll-downs_mynext go_to" href=".next2">
					<div class="scroll-downs">
					  <div class="mousey">
					    <div class="scroller"></div>
					  </div>
					</div>
                </button>
	<div class="slider_alltop">
		<?php foreach($main_slider as $sliders):?>
			<div class="slider_alltop_1" style="background-image: url(<?=$sliders->image_url.'/'.$sliders->image_path?>);">
				<div class="slider_alltop_1_par">
					<div class="slider_alltop_1_par_vh">
						<h1><?=Html::encode($sliders->firstTitle).'<br />';?>
		                    <span><?=Html::encode($sliders->secondTitle);?></span>
		                </h1>
		                <p><?=Html::encode($sliders->text);?></p>
	                </div>
				</div>
			</div>
		<?php endforeach;?>
	</div>
</section>
<!-- compony -->
<section class="compony next2" style="background-image: url(<?=$about_company->image_url.'/'.$about_company->image_path?>);">
	<div class="container">
		<div class="compony_all">
			<div class="all_top_h1s">
			    <h1>О компании</h1>
			    <div class="all_top_h1s_div"></div>
		    </div>
		    <div class="compony_all_p">
		    	<p><?=Html::encode($about_company->title);?></p>
		    	<p><?=Html::encode($about_company->text);?></p>
		    </div>
		    <div class="compony_all_3divs">

		    	<?php foreach($company_icons as $icons):?>
			    	<div class="compony_all_3divs-1">
			    		<div class="compony_all_3divs-1-top">
			    			<img src="<?=$icons->icon_url.'/'.$icons->icon_path?>">
			    		</div>
			    		<div class="compony_all_3divs-1-bot">
			    			<h6><?=Html::encode($icons->title);?></h6>
			    			<p><?=Html::encode($icons->text);?></p>
			    		</div>
			    	</div>
		    	<?php endforeach;?>

		    </div>
		</div>
	</div>
</section>
<!-- production -->
<section class="production next3" style="background-image: url(<?=$producing_part->image_url.'/'.$producing_part->image_path?>);">
	<div class="container">
		<div class="production_all">
			<div class="all_top_h1s">
			    <h1>ПРОИЗВОДСТВО</h1>
			    <div class="all_top_h1s_div"></div>
		    </div>
		    <div class="compony_all_p">
		    	<p><?=Html::encode($producing_part->title)?></p>
		    	<p><?=Html::encode($producing_part->text)?></p>
		    </div>

		    <div class="compony_all_3div">
		    	<?php foreach($producing_icons as $pr_icons):?>
		    	<div class="compony_all_3div-1">
		    		<div class="compony_all-lit_left"></div>
		    		<div class="compony_all-lit_right">
		    			<h6><?=Html::encode($pr_icons->title)?></h6>
		    			<p><?=Html::encode($pr_icons->text)?></p>
		    		</div>
		    	</div>
		    	<?php endforeach;?>

		    </div>
		</div>
	</div>
</section>
<!-- cloth -->
<section class="cloth next4">
	<div class="container">
        <div class="cloth_all">
			<div class="all_top_h1s">
			    <h1>НАША ПРОДУКЦИЯ</h1>
			    <div class="all_top_h1s_div"></div>
		    </div>
			<div class="cloth_slick">
				<div class="center">
				<?php $counter = 0;?>
					<?php foreach($our_production as $productions):?>
					<?php $counter++;?>
					<div  class="cloth_slick1 <?php if($counter == 1) {echo 'active_cloth';}?>" data-id="<?=$productions->id?>"  >
						<div class="cloth_slick1-ckang-ckang">
							<div class="img_cloth_slick1" style="background-image: url(<?=$productions->image_url.'/'.$productions->image_path?>);"></div>
							<div class="cloth_slick1_h6">
								<h6><?=$productions->firstTitle?><br><?=$productions->secondTitle?></h6>
							</div>
							<p><?=$productions->text?></p>
							<button class="bot__2 go_to our_production_btn" data-id="<?=$productions->id?>" href=".s1">ПОКАЗАТЬ КОЛЛЕКЦИЮ</button>

						</div>
						<div class="img-pos-bot_">
						    <img src="/orianna/img/clothimg2.png">
						</div>
					</div>
					<?php endforeach;?>

				</div>
			</div>
			<!-- range flow images -->
			<div class="stuff s1 our_production_area coverflow" style="margin: 90px 0 50px 0">

                <ul class="flip-items">
             	<?php foreach($our_production as $k => $productions):?>
                	<?php if($k === 0):?>
                	    <?php foreach($productions->productImages as $item):?>
                	    <?php if($item):?>
                        <li><img src="<?=$item->image_url.'/'.$item->image_path?>" alt=""></li>
                        <?php endif;?>
                        <?php endforeach?>
                    <?php endif;?>
                <?php endforeach; ?>
                </ul>

                <div class="flip-desc">
             	<?php foreach($our_production as $k => $productionsds):?>
                	<?php if($k === 0):?>
                	    <?php foreach($productionsds->productImages as $item):?>
                	    <?php $max = $productionsds->productImages ? count($productionsds->productImages) : 0?>
                	    <?php if($item):?>
                	    <div style="text-align: center">
                            <span class="image-code"><?= $item->code ?></span>
                	    </div>

                        <?php endif;?>
                        <?php endforeach?>
                    <?php endif;?>
                <?php endforeach; ?>
                </div>
			</div>
			<input type="range" id="range" min="0" max="<?=$max-1?>" value="0">
        </div>
	</div>
</section>
<!-- proizvostva -->
<section class="proizvostva">
	<div class="proizvostva_all">
		<div class="all_top_h1s">
	        <h1>ГАЛЕРЕЯ ПРОИЗВОДСТВА</h1>
	        <div class="all_top_h1s_div"></div>
        </div>
        <div class="proizvostva_imgs">
        	<div id="lightgallery">
        		<?php foreach($gallery as $items):?>
        	    <a class="proizvostva_img1"  href="<?=$items->image_url.'/'.$items->image_path?>">
            	 	<div class="lightgallery_bac-img_1" style="background-image: url(<?=$items->image_url.'/'.$items->image_path?>);">

						<div class="ln1overlay">
						    <div class="ln1text">
						        <div class="ln1text_insate">
						        	<p><?=$items->title?></p>
						    	     <i class="fa fa-search" aria-hidden="true"></i>
						        </div>
						    </div>
						</div>

            	 	</div>
            	</a>
           		<?php endforeach?>
            </div>
        </div>
	</div>
</section>
<!-- zadacha -->
<section class="zadacha" style="background-image: url(<?=$company_advantages->image_url.'/'.$company_advantages->image_path?>);">
	<div class="container">
		<div class="zadacha_all">
		    <div class="all_top_h1s">
	            <h1>Преимущества компании</h1>
	            <div class="all_top_h1s_div"></div>
            </div>
            <div class="zadacha_p-top">
            	<p><?=$company_advantages->text?></p>
            </div>
            <div class="zadacha_4divs">

            	<?php foreach($advantage_icons as $items):?>
	            	<div class="zadacha_4divs-1">
	            		<div class="zadacha_4divs-1_left">
	            			<span>
	            				<img src="<?=$items->icon_url.'/'.$items->icon_path?>">
	            			</span>
	            		</div>
	            		<div class="zadacha_4divs-1_right">
	            			<h6><?=$items->title?></h6>
	            			<p><?=$items->text?></p>
	            		</div>
	            	</div>
            	<?php endforeach;?>
            </div>
		</div>
	</div>
</section>
<!-- newsnov -->
<section class="newsnov next5">
	<div class="container">
		<div class="newsnov_all">
			<div class="all_top_h1s">
	            <h1>НОВОСТИ КОМПАНИИ</h1>
	            <div class="all_top_h1s_div"></div>
            </div>
            <div class="newsnov_3dovs">
	            <?php foreach($company_news as $items):?>
	            	<div class="newsnov_3dovs-1">
	            		<div class="newsnov_-1-img" style="background-image: url(<?=$items->image_url.'/'.$items->image_path?>);"></div>
	            		<div class="newsnov_-1-cont">
	            			<h6><?=Yii::$app->formatter->asDate($items->created_at, 'php:d F, Y')?></h6>
	            			<h5><?=$items->title?></h5>
	            			<p><?=$items->text?></p>
	            			<a href="<?=Url::to(['/site/one-news-open', 'id' => $items->id])?>">УЗНАТЬ БОЛЬШЕ</a>
	            		</div>
	            	</div>
	            <?php endforeach;?>
            </div>
            <div class="read_more">
            	<a href="<?=Url::to(['/site/all-news'])?>">ВСЕ НОВОСТИ</a>
            </div>
		</div>
	</div>
</section>
<!-- partner -->
<section class="portner">
	<div class="container">
		<div class="portner_all">
			<p>Мы приглашаем заказчиков, дилеров и бизнес партнеров к взаимовыгодному
            и долгосрочному сотрудничеству!</p>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >СВЯЗАТЬСЯ С НАМИ</button>
		</div>
	</div>
</section>
<!-- map -->
<section class="mapb">
	<div class="mapb_all">

	  <ul class="nav nav-tabs">

	    <li class="active tashkent_map">
	    	<a data-toggle="tab" href="#home">
	    		<span><i class="fa fa-map-marker" aria-hidden="true"></i> </span> МЫ В ТАШКЕНТЕ
	    	</a>
	    </li>

	    <li class="samarkand_map">
	    	<a data-toggle="tab" href="#menu1"><span><i class="fa fa-map-marker" aria-hidden="true"></i> </span> МЫ В НАВОИ
	    	</a>
	    </li>

	  </ul>

	  <div class="tab-content">

	    <div id="tashkent_map" class="tab-pane fade in active" style="height: 620px; width: 100%">

	    </div>

	    <div id="samarkand_map" class="tab-pane fade" style="height: 620px; width: 100%">

	    </div>

	  </div>

	</div>
</section>


<?php
	$this->registerJs("

		$(function(){

			main_map = L.map('tashkent_map').setView([41.278307, 69.253618], 16);
	        mapTileLayer =   L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	            maxZoom: 18,
	            id: 'mapbox.streets',
	            accessToken: 'pk.eyJ1IjoicG9tbXkiLCJhIjoiY2pjZzByOXh5MTFhaTJ5cnZncnhrMGxobCJ9.ebYK9RNn-z-5MvoMt5a-IA'
	        }).addTo(main_map);

	        tashkent_location = new  L.latLng(41.278307, 69.253618);

	       	custom_icon = L.icon({ 
	            iconUrl: '/orianna/img/dest.png',
	            iconSize: [45, 60],
	        });

			var marker = new L.marker(tashkent_location, {icon: custom_icon});
			marker.addTo(main_map);


			$('body').on('click', '.tashkent_map', function(){
				main_map.flyTo([41.278307, 69.253618], 16, {duration: 0.8,});
				marker.setLatLng(tashkent_location); 
			})


			$('body').on('click', '.samarkand_map', function(){
				samarkand_location = new  L.latLng(41.588872, 64.208109);
				main_map.flyTo([41.588872, 64.208109], 16, {duration: 0.8,});
				marker.setLatLng(samarkand_location); 
			})


			$('.cloth_slick1').each(function(index, value){
				if($(value).hasClass('active_cloth')){
					var production_id = $(value).data('id');
					$.ajax({
				    	url: '/site/production-images',
				    	data: {production_id: production_id},
				    	dataType: 'json',
				    	success: function(response){
				    		$('.our_production_area').html(response);
				    		imageFlowInit();
				    	}
				    })
				}
			})
			

		})


		function imageFlowInit(){
			if ($( window ).width() >= 760) {
		      var instanceOne = new ImageFlow();

		      console.log($('#myImageFlow_images img'));

		      instanceOne.init({ ImageFlowID: 'myImageFlow', startID: 3 });
		    } else {
		      var circular_3 = new ImageFlow();
		      circular_3.init({
		          ImageFlowID: 'myImageFlow',
		          // circular: true,
		          reflections: false,
		          reflectionP: 1,
		          slider: false,
		          captions: true,
		          opacity: true,
		          xStep: 300,
		          // imageFocusM: 5,
		          // imagesHeight: 1,
		          // sliderWith: 500,
		          // imageFocusMax: 1,
		          startID: 3
		      });
		    }
		}

	", \yii\web\View::POS_END);
?>