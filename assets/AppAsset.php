<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'libs/bootstrap/dist/css/bootstrap.min.css',
        'libs/font-awesome-4.7.0/css/font-awesome.min.css',
        'libs/fancybox-master/dist/jquery.fancybox.min.css',
        'libs/slick/slick.css',
        'libs/slick/slick-theme.css',
        'css/style.min.css',
        'css/site.css',
        'css/nemo.css'
    ];
    public $js = [
        'libs/bootstrap/dist/js/bootstrap.min.js',
        'libs/slick/slick.min.js',
        'libs/fancybox-master/dist/jquery.fancybox.min.js',
        'js/common.js',
        'js/swal.js',
        'js/myjs.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
