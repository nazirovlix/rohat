<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AboutUs */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'About uses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-us-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'image1',
            'about_us_ru:ntext',
            'about_us_uz:ntext',
            'about_us_en:ntext',
            'image2',
            'our_mission_ru:ntext',
            'our_mission_uz:ntext',
            'our_mission_en:ntext',
            'image3',
            'why_us_ru:ntext',
            'why_us_uz:ntext',
            'why_us_en:ntext',
        ],
    ]) ?>

</div>
