<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Offers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'base_file')->widget(trntv\filekit\widget\Upload::className(), ['url' => app\models\BaseModel::FILE_UPLOAD_URL, 'maxFileSize' => app\models\BaseModel::MAX_FILE_UPLOAD_SIZE]) ?>

    <?= $form->field($model, 'airlines_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'airlines_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'airlines_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'flight_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'flight_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'flight_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transfer_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transfer_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transfer_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'discount')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([app\models\BaseModel::STATUS_ACTIVE => Yii::t('app', 'Published'), app\models\BaseModel::STATUS_INACTIVE => Yii::t('app', 'Not Published')]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
