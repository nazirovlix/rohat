<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ContactsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'phone1') ?>

    <?= $form->field($model, 'address1_ru') ?>

    <?= $form->field($model, 'address1_uz') ?>

    <?= $form->field($model, 'address1_en') ?>

    <?php // echo $form->field($model, 'image_scheme1') ?>

    <?php // echo $form->field($model, 'phone2') ?>

    <?php // echo $form->field($model, 'address2_ru') ?>

    <?php // echo $form->field($model, 'address2_uz') ?>

    <?php // echo $form->field($model, 'address2_en') ?>

    <?php // echo $form->field($model, 'image_scheme2') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'work_time') ?>

    <?php // echo $form->field($model, 'url_playmarket') ?>

    <?php // echo $form->field($model, 'urlappstore') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
