<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contacts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'phone1')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address1_ru')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address1_uz')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address1_en')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'phone3')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col">
            <?= $form->field($model, 'phone2')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address2_ru')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address2_uz')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address2_en')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'phone4')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'base_file')->widget(
                trntv\filekit\widget\Upload::className(),
                [
                    'url' => app\models\BaseModel::FILE_UPLOAD_URL,
                    'maxFileSize' => app\models\BaseModel::MAX_FILE_UPLOAD_SIZE
                ]) ?>

        </div>
        <div class="col">
            <?= $form->field($model, 'base_file1')->widget(
                trntv\filekit\widget\Upload::className(),
                [
                    'url' => app\models\BaseModel::FILE_UPLOAD_URL,
                    'maxFileSize' => app\models\BaseModel::MAX_FILE_UPLOAD_SIZE
                ]) ?>
        </div>
    </div>




    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'work_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url_playmarket')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'urlappstore')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
