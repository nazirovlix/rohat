<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contacts */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contacts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'phone1',
            'address1_ru',
            'address1_uz',
            'address1_en',
            [
                'attribute' => 'image_scheme1',
                'value' => function ($model) {
                    return Html::img('/uploads/' . $model->image_scheme1, ['style' => 'max-width:100px']);
                },
                'format' => 'html'
            ],
            'phone2',
            'address2_ru',
            'address2_uz',
            'address2_en',
            [
                'attribute' => 'image_scheme2',
                'value' => function ($model) {
                    return Html::img('/uploads/' . $model->image_scheme2, ['style' => 'max-width:100px']);
                },
                'format' => 'html'
            ],
            'email:email',
            'work_time',
            'url_playmarket:url',
            'urlappstore',
        ],
    ]) ?>

</div>
