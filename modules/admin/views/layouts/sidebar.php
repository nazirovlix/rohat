<?php
/**
 * Created by PhpStorm.
 * User: Farhodjon
 * Date: 10.03.2018
 * Time: 15:17
 */

use app\modules\admin\widgets\Menu;
use yii\helpers\Url;

?>
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <?php
            try {
                echo Menu::widget([
                    'options' => ['id' => 'sidebarnav'],
                    'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                    'badgeClass' => 'label label-rouded label-primary pull-right',
                    'activateParents' => true,
                    'items' => [
                        [
                            'label' => '',
                            'options' => ['class' => 'nav-devider']
                        ],
                        [
                            'label' => 'Home',
                            'options' => ['class' => 'nav-label']
                        ],
                        [
                            'label' => 'Dashboard',
                            'url' => ['default/index'],
                            'icon' => '<i class="fa fa-tachometer"></i>',
                        ],
                        [
                            'label' => 'App',
                            'options' => ['class' => 'nav-label']
                        ],
                        [
                            'label' => 'Home page',
                            'url' => '#',
                            'icon' => '<i class="fa fa-home"></i>',
                            'items' => [
                                [
                                    'label' => 'Carousel',
                                    'url' => ['carousel/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Offers',
                                    'url' => ['offers/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Partners',
                                    'url' => ['partners/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Follows',
                                    'url' => ['follow/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Callback',
                                    'url' => ['callback/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Feedback',
                                    'url' => ['feedback/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                                [
                                    'label' => 'Social networks',
                                    'url' => ['socials/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                            ]
                        ],
                        [
                            'label' => 'About',
                            'url' => '#',
                            'icon' => '<i class="fa fa-info"></i>',
                            'items' => [
                                [
                                    'label' => 'About Us',
                                    'url' => ['about-us/update', 'id' => 1],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Contacts',
                                    'url' => ['contacts/update', 'id' => 1],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                            ]
                        ],
                        [
                            'label' => 'Pages',
                            'url' => '#',
                            'icon' => '<i class="fa  fa-file-o"></i>',
                            'items' => [

                                [
                                    'label' => 'News',
                                    'url' => ['news/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Albums',
                                    'url' => ['album/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Faq Categories',
                                    'url' => ['faq-categories/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Faqs',
                                    'url' => ['faqs/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Pages',
                                    'url' => ['pages/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Advantages',
                                    'url' => ['advantages/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],


                            ]
                        ],

                    ]
                ]);
            } catch (Exception $e) {
            }

            ?>
        </nav>
    </div>
</div>
