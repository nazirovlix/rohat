<!-- Start Page Content -->
<div class="row">
    <div class="col-md-4">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-facebook f-s-40 color-primary"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $feedback ?></h2>
                    <p class="m-b-0">Total feedback</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-phone f-s-40 color-success"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $callback ?></h2>
                    <p class="m-b-0">Total callback</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-newspaper-o f-s-40 color-warning"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $news ?></h2>
                    <p class="m-b-0">News</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title">
                <h4>Recent Orders </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Created at</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if (!empty($feed)) {

                            foreach ($feed as $k => $item): ?>
                                <tr>
                                    <td><?= ++$k ?></td>
                                    <td><?= $item->fio ?></td>
                                    <td><?= $item->company ?></td>
                                    <td><?= $item->email ?></td>
                                    <td><?= $item->message ?></td>
                                    <td><?= date('d.m.Y H:i', $item->created_at) ?></td>
                                </tr>
                            <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- End PAge Content -->