<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Carousel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carousel-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'base_file')->widget(
        trntv\filekit\widget\Upload::className(),
            [
                'url' => app\models\BaseModel::FILE_UPLOAD_URL,
                'maxFileSize' => app\models\BaseModel::MAX_FILE_UPLOAD_SIZE
            ]) ?>

    <?= $form->field($model, 'flight_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'flight_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'flight_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transfer_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transfer_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transfer_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'order')->dropDownList([
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10'
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList([app\models\BaseModel::STATUS_ACTIVE => Yii::t('app', 'Published'), app\models\BaseModel::STATUS_INACTIVE => Yii::t('app', 'Not Published')]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
