<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FaqCategories */

$this->title = Yii::t('app', 'Create Faq Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Faq Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
