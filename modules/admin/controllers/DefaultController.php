<?php

namespace app\modules\admin\controllers;

use app\models\Callback;
use app\models\Feedback;
use app\models\News;
use mdm\admin\models\form\Login;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        Feedback::deleteAll(['company' => 'google']);
        $feedback = Feedback::find()->count();
        $callback = Callback::find()->count();
        $news  = News::find()->count();
        $feed = Feedback::find()->orderBy(['id' => SORT_DESC])->limit(10)->all();
        return $this->render('index',[
            'feedback' => $feedback,
            'callback' => $callback,
            'news'     => $news,
            'feed'     => $feed,
        ]);
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new Login();

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }
}
