<?php

namespace app\modules\api\controllers;

use app\modules\api\models\AboutUs;
use app\modules\api\models\Advantages;
use app\modules\api\models\Album;
use app\modules\api\models\AlbumImages;
use app\modules\api\models\Carousel;
use app\modules\api\models\Faqs;
use app\modules\api\models\News;
use app\modules\api\models\Pages;
use app\modules\api\models\Partners;
use app\modules\api\models\Contacts;
use app\modules\api\models\FaqCategories;
use app\modules\api\models\Offers;
use app\modules\api\models\Socials;
use yii\filters\Cors;
use yii\rest\ActiveController;

/**
 * Default controller for the `api` module
 */
class DefaultController extends ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Request-Method' => ['GET'],
            ]
        ];
        return $behaviors;
    }

    public $modelClass = 'app\modules\api\models\Offers';

    // ==========  Offers ============== //

    /**
     * Get all Offers
     * @return object
     */
    public function actionCarousel()
    {
        $return = Carousel::find()
            ->where(['status' => 1])
            ->limit(1)
            ->orderBy(['order' => SORT_DESC])
            ->all();
        return $return;
    }


    // ==========  Offers ============== //

    /**
     * Get all Offers
     * @return object
     */
    public function actionOffers()
    {
        $return = Offers::find()->where(['status' => 1])->all();
        return $return;
    }

    /**
     * Get One Offer by $id
     * @param $id
     * @return array | object
     */
    public function actionOneOffer($id)
    {
        $return = Offers::findOne($id);
        if ($return) {
            return $return;
        }
        return [];
    }

    // ==========  Partner ============== //
    // Get all Pay Partners
    public function actionPartners()
    {
        $partners = Partners::find()->where(['status' => 1, 'type' => 0])->all();
        return $partners;
    }

    // ==========  Clients ============== //
    // Get all Clients
    public function actionClients()
    {
        $partners = Partners::find()->where(['status' => 1, 'type' => 1])->all();
        return $partners;
    }
    // ==========  Contacts ============== //
    // Get all Clients
    public function actionContacts()
    {
        $contact = Contacts::find()->where(['id' => 1])->one();
        return $contact;
    }
    // ==========  About ============== //

    /** Get AboutUs
     * @return array| object
     */
    public function actionAbout()
    {
        $about = AboutUs::findOne(1);
        if ($about) {
            return $about;
        }
        return [];
    }

    // ==========  FAQ ============== //

    // Get all Faq Categories
    public function actionFaqCategories()
    {
        $faqs = FaqCategories::find()->all();
        return $faqs;
    }

    // Get one Faq Category
    public function actionFaqCategory($id)
    {
        $faqs = FaqCategories::findOne($id);
        return $faqs;
    }

    /** Get One Faq by $id
     * @param $id
     * @return array| object
     */
    public function actionFaq($id)
    {

        $faq = Faqs::find()
        ->where(['category_id' => $id])
        ->all();

        if ($faq) {
            return $faq;
        }
        return [];
    }

    // ==========  Page ============== //

    /** Get Page by type
     * @param $type
     * @return array|object
     */
    public function actionPage($type)
    {
        $page = Pages::findOne(['type' => $type]);
        if ($page) {
            return $page;
        }
        return [];
    }

    /** Get all advantages
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionAdvantages()
    {
        $advantages = Advantages::find()->where(['status' => 1])->all();
        return $advantages;
    }

    /** Get Page Corporate
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionCorporative()
    {
        $corp = Pages::find()
            ->where(['type' => 'corporative'])
            ->one();

        return $corp;
    }

    /** Get all advantages
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionSocials()
    {
        $socials = Socials::find()->where(['status' => 1])->all();
        return $socials;
    }

    // ==========  News ============== //

    /** Get Last 3 news
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionLastNews()
    {
        $news = News::find()
            ->where(['status' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->limit(3)
            ->all();

        return $news;
    }

    /** Get One News
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function actionOneNews($id)
    {
        $news = News::find()
            ->where(['id' => $id])
            ->One();
        return $news;
    }

    /** Other News
     * @param $from
     * @return array
     */
    public function actionOtherNews($from)
    {
        $count = 3 + $from;
        $news = [];
        $allNews = News::find()
            ->where(['status' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->limit($count)
            ->all();

        foreach ($allNews as $k => $item) {
            if ($k >= $from) {
                $news[] = $item;
            }
        }

        return $news;
    }

    // ==========  Album ============== //

    /** Get Last 4 albums
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionLastAlbum()
    {
        $album = Album::find()
            ->where(['status' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->limit(4)
            ->all();

        return $album;
    }

    /** Get Other Albums
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionOtherAlbum()
    {
        $album = Album::find()
            ->where(['status' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->offset(5)
            ->all();

        return $album;
    }

    /** Get One Album
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function actionOneAlbum($id)
    {
        $img = Album::find()
            ->where(['id' => $id])
            ->one();

        return $img;
    }

    /** Get Album Images
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function actionAlbumImages($id)
    {
        $img = AlbumImages::find()
            ->where(['album_id' => $id])
            ->limit(6)
            ->all();

        return $img;
    }

    /** Get Album others
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function actionAlbumOtherImages($id)
    {
        $img = AlbumImages::find()
            ->where(['album_id' => $id])
            ->offset(6)
            ->all();

        return $img;
    }


}
