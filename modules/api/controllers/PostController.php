<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 07.01.2019
 * Time: 18:37
 */

namespace app\modules\api\controllers;

use app\models\Carousel;
use app\modules\api\models\Callback;
use app\modules\api\models\Feedback;
use app\modules\api\models\Follow;
use app\modules\api\models\Offers;
use Yii;
use yii\filters\Cors;
use yii\rest\ActiveController;

class PostController extends ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST'],
            ]
        ];
        return $behaviors;
    }

    public $modelClass = 'app\modules\api\models\Offers';

    /** Create Callback
     * @return bool
     */
    public function actionCallBack()
    {
        $callback = new Callback();
        $callback->fio      = Yii::$app->request->post('fio');
        $callback->phone    = Yii::$app->request->post('phone');
        $callback->status   = 0;
        if ($callback->save()) {
            return true;
        }
        return false;

    }


    /** Create Feedback
     * @return bool
     */
    public function actionFeedBack()
    {
        $callback = new Feedback();
        $callback->fio      = Yii::$app->request->post('fio');
        $callback->company  = Yii::$app->request->post('company');
        $callback->email    = Yii::$app->request->post('email');
        $callback->message  =  Yii::$app->request->post('message');
        $callback->status   = 0;
        if ($callback->save()) {
            return true;
        }
        return false;
    }

    /** Create Subscribe
     * @return bool
     */
    public function actionSubscribe()
    {
        $callback = new Follow();
        $callback->name     = Yii::$app->request->post('fio');
        $callback->email    = Yii::$app->request->post('email');
        $callback->status   = 0;
        if ($callback->save()) {
            return true;
        }
        return false;
    }





}