<?php

namespace app\modules\api\models;

use Yii;

/**
 * This is the model class for table "faqs".
 *
 * @property int $id
 * @property int $category_id
 * @property string $question_ru
 * @property string $question_uz
 * @property string $question_en
 * @property string $answer_ru
 * @property string $answer_uz
 * @property string $answer_en
 * @property int $status
 * @property int $order
 * @property int $created_at
 * @property int $updated_at
 *
 * @property FaqCategories $category
 */
class Faqs extends \app\models\Faqs
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
