<?php

namespace app\modules\api\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "follow".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Follow extends \app\models\Follow
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
