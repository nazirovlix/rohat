<?php

namespace app\modules\api\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_uz
 * @property string $title_en
 * @property string $content_ru
 * @property string $content_uz
 * @property string $content_en
 * @property string $image_main
 * @property string $image_little
 * @property string $slug
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class News extends \app\models\News
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
