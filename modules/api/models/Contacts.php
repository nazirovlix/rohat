<?php

namespace app\modules\api\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $phone1
 * @property string $address1_ru
 * @property string $address1_uz
 * @property string $address1_en
 * @property string $image_scheme1
 * @property string $phone2
 * @property string $address2_ru
 * @property string $address2_uz
 * @property string $address2_en
 * @property string $image_scheme2
 * @property string $email
 * @property string $work_time
 * @property string $url_playmarket
 * @property string $urlappstore
 */
class Contacts extends \app\models\Contacts
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
