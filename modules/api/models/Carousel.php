<?php
namespace app\modules\api\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "carousel".
 *
 * @property int $id
 * @property string $image
 * @property string $flight_ru
 * @property string $flight_uz
 * @property string $flight_en
 * @property string $transfer_ru
 * @property string $transfer_uz
 * @property string $transfer_en
 * @property int $price
 * @property int $order
 * @property int $status
 */
class Carousel extends \app\models\Carousel
{

    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }

}
