<?php
namespace app\modules\api\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $fio
 * @property string $company
 * @property string $email
 * @property string $message
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Feedback extends \app\models\Feedback
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
