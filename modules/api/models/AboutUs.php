<?php

namespace app\modules\api\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "about_us".
 *
 * @property int $id
 * @property string $image1
 * @property string $about_us_ru
 * @property string $about_us_uz
 * @property string $about_us_en
 * @property string $image2
 * @property string $our_mission_ru
 * @property string $our_mission_uz
 * @property string $our_mission_en
 * @property string $image3
 * @property string $why_us_ru
 * @property string $why_us_uz
 * @property string $why_us_en
 */
class AboutUs extends \app\models\AboutUs
{

    public function fields()
    {
        $fields = parent::fields();

        return $fields;
    }
}
