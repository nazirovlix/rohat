<?php

namespace app\modules\api\models;

use Yii;

/**
 * This is the model class for table "album_images".
 *
 * @property int $id
 * @property int $album_id
 * @property string $img
 * @property int $sort
 *
 * @property Album $album
 */
class AlbumImages extends \app\models\AlbumImages
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
