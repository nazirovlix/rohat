<?php

namespace app\modules\api\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 *
 * @property mixed $name
 * @property mixed $description
 * @property mixed $title
 * @property mixed $content
 */
class BaseModel extends \app\models\BaseModel
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }


}