<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 09.01.2019
 * Time: 10:18
 */

namespace app\modules\api\models;

class Offers extends \app\models\Offers
{
    public function fields()
    {
        $fields = parent::fields();
        unset($fields['image']);
        unset($fields['created_at']);
        unset($fields['updated_at']);

        return $fields;
    }

}