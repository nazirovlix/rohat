<?php

namespace app\modules\api\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $url
 * @property int $status
 */
class Partners extends \app\models\Partners
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
