<?php

namespace app\modules\api\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "faq_categories".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_uz
 * @property string $name_en
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Faqs[] $faqs
 */
class FaqCategories extends \app\models\FaqCategories
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
