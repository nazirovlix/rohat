<?php

namespace app\modules\api\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "advantages".
 *
 * @property int $id
 * @property string $image
 * @property string $title_ru
 * @property string $title_uz
 * @property string $title_en
 * @property int $status
 */
class Advantages extends \app\models\Advantages
{
    public function fields()
    {
        $fields = parent::fields();

        return $fields;
    }
}
