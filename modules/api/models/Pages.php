<?php

namespace app\modules\api\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $slug
 * @property string $title_ru
 * @property string $title_uz
 * @property string $title_en
 * @property string $content_ru
 * @property string $content_uz
 * @property string $content_en
 * @property int $type
 * @property int $created_at
 * @property int $updated_at
 */
class Pages extends \app\models\Pages
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
