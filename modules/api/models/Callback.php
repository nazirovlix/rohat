<?php

namespace app\modules\api\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "callback".
 *
 * @property int $id
 * @property string $fio
 * @property string $phone
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Callback extends \app\models\Callback
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
