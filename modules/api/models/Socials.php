<?php

namespace app\modules\api\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "socials".
 *
 * @property int $id
 * @property string $url
 * @property string $image
 * @property int $order
 * @property int $status
 */
class Socials extends \app\models\Socials
{
    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}
