$('#feedbackFrom').submit(function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
   var form = $(this);
   var ser = form.serialize();
   console.log(ser);

   $.ajax({
       url:'/site/feedback',
       data: ser,
       type: 'post',
       success: function (res) {
           if(res == true){
               $('#modal-req').modal('toggle');
               swal('Спасибо','Ваша заявка принята','success');
               return false;
           } else {
               swal('Ошибка!','Пожалуйста, заполните все поля правильно','warning');
               return false;
           }
       },
       error: function () {
           alert("Error")
       }
   })

});
$('#callbackFrom').submit(function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var form = $(this);
    var ser = form.serialize();
    console.log(ser);

    $.ajax({
        url:'/site/callback',
        data: ser,
        type: 'post',
        success: function (res) {
            if(res == true){
                $('#phone-modal').modal('toggle');
                swal('Спасибо','Ваша заявка на звонок принята','success');
                return false;
            } else {
                swal('Ошибка!','Пожалуйста, заполните все поля правильно','warning');
                return false;
            }
        },
        error: function () {
            alert("Error")
        }
    })

});