<?php

use yii\db\Migration;

/**
 * Handles the creation of table `offers`.
 */
class m181218_063949_create_offers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('offers', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'airlines_ru' => $this->string()->notNull(),
            'airlines_uz' => $this->string()->notNull(),
            'airlines_en' => $this->string()->notNull(),
            'flight_ru' => $this->string(),
            'flight_uz' => $this->string(),
            'flight_en' => $this->string(),
            'transfer_ru' => $this->string(),
            'transfer_uz' => $this->string(),
            'transfer_en' => $this->string(),
            'price' => $this->integer(),
            'discount' => $this->integer(),
            'slug' => $this->string(),
            'status' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('offers');
    }
}
