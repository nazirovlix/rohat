<?php

use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m181218_101448_create_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(),
            'company' => $this->string(),
            'email' => $this->string(),
            'message' => $this->string(),
            'status' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('feedback');
    }
}
