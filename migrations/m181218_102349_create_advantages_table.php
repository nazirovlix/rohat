<?php

use yii\db\Migration;

/**
 * Handles the creation of table `advantages`.
 */
class m181218_102349_create_advantages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('advantages', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'title_ru' => $this->string()->notNull(),
            'title_uz' => $this->string()->notNull(),
            'title_en' => $this->string()->notNull(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('advantages');
    }
}
