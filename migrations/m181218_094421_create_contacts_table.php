<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m181218_094421_create_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'phone1' => $this->string(),
            'address1_ru' => $this->string(),
            'address1_uz' => $this->string(),
            'address1_en' => $this->string(),
            'image_scheme1' => $this->string(),
            'phone2' => $this->string(),
            'address2_ru' => $this->string(),
            'address2_uz' => $this->string(),
            'address2_en' => $this->string(),
            'image_scheme2' => $this->string(),
            'email' => $this->string(),
            'work_time' => $this->string(),
            'url_playmarket' => $this->string(),
            'urlappstore' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contacts');
    }
}
