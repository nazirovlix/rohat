<?php

use yii\db\Migration;

/**
 * Handles the creation of table `socials`.
 */
class m181219_061446_create_socials_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('socials', [
            'id' => $this->primaryKey(),
            'url' => $this->string(),
            'image' => $this->string(),
            'order' => $this->smallInteger(),
            'status' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('socials');
    }
}
