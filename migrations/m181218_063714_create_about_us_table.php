<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_us`.
 */
class m181218_063714_create_about_us_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('about_us', [
            'id' => $this->primaryKey(),
            'image1' => $this->string(),
            'about_us_ru' => $this->text(),
            'about_us_uz' => $this->text(),
            'about_us_en' => $this->text(),
            'image2' => $this->string(),
            'our_mission_ru' => $this->text(),
            'our_mission_uz' => $this->text(),
            'our_mission_en' => $this->text(),
            'image3' => $this->string(),
            'why_us_ru' => $this->text(),
            'why_us_uz' => $this->text(),
            'why_us_en' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('about_us');
    }
}
