<?php

use yii\db\Migration;

/**
 * Handles the creation of table `callback`.
 */
class m181219_103539_create_callback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('callback', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(),
            'phone' => $this->string(),
            'status' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('callback');
    }
}
