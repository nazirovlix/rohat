<?php

use yii\db\Migration;

/**
 * Handles the creation of table `carousel`.
 */
class m181218_063401_create_carousel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('carousel', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'flight_ru' => $this->string(),
            'flight_uz' => $this->string(),
            'flight_en' => $this->string(),
            'transfer_ru' => $this->string(),
            'transfer_uz' => $this->string(),
            'transfer_en' => $this->string(),
            'price' => $this->integer(),
            'order' => $this->smallInteger(),
            'status' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('carousel');
    }
}
