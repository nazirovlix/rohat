<?php

use yii\db\Migration;

/**
 * Handles the creation of table `faqs`.
 * Has foreign keys to the tables:
 *
 * - `faq_categories`
 */
class m181218_101856_create_faqs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('faqs', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'question_ru' => $this->string(),
            'question_uz' => $this->string(),
            'question_en' => $this->string(),
            'answer_ru' => $this->text(),
            'answer_uz' => $this->text(),
            'answer_en' => $this->text(),
            'status' => $this->smallInteger(),
            'order' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            'idx-faqs-category_id',
            'faqs',
            'category_id'
        );

        // add foreign key for table `faq_categories`
        $this->addForeignKey(
            'fk-faqs-category_id',
            'faqs',
            'category_id',
            'faq_categories',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `faq_categories`
        $this->dropForeignKey(
            'fk-faqs-category_id',
            'faqs'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            'idx-faqs-category_id',
            'faqs'
        );

        $this->dropTable('faqs');
    }
}
