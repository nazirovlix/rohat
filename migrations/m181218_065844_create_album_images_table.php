<?php

use yii\db\Migration;

/**
 * Handles the creation of table `album_images`.
 * Has foreign keys to the tables:
 *
 * - `album`
 */
class m181218_065844_create_album_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('album_images', [
            'id' => $this->primaryKey(),
            'album_id' => $this->integer()->notNull(),
            'img' => $this->string(),
            'sort' => $this->integer(),
        ]);

        // creates index for column `album_id`
        $this->createIndex(
            'idx-album_images-album_id',
            'album_images',
            'album_id'
        );

        // add foreign key for table `album`
        $this->addForeignKey(
            'fk-album_images-album_id',
            'album_images',
            'album_id',
            'album',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `album`
        $this->dropForeignKey(
            'fk-album_images-album_id',
            'album_images'
        );

        // drops index for column `album_id`
        $this->dropIndex(
            'idx-album_images-album_id',
            'album_images'
        );

        $this->dropTable('album_images');
    }
}
