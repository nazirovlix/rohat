<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\BaseModel;
use app\models\Callback;
use app\models\Feedback;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" type="image/png" href="/images/favicon.png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript">
        function superkassaInit(iframe) {
           if (document && document.body) {
               document.body.style.margin = '0';
           }

            var isPopupOpened = false;
            var height;
            var scroll;
            window.addEventListener('message', function(event) {
                var data = JSON.parse(event.data);
                if (data.from == 'superkassa') {
                    switch (data.message) {
                        case 'openPopup':
                            isPopupOpened = true;
                            scroll = window.scrollY;
                            height = iframe.style.height;
                            console.log(scroll);
                            if (window) {
                                window.scrollTo(0, scroll);
                            }
                            iframe.style.height = screen.availHeight + 'px';
                            if (document && document.body) {
                                document.body.style.overflow = 'hidden';
                            }
                            break;
                        case 'closePopup':

                            isPopupOpened = false;
                            iframe.style.height = height;
                            if (document && document.body) {
                                document.body.style.overflow = 'auto';
                            }
                            if (window) {
                                window.scrollTo(0, scroll);
                            }
                            break;
                        case 'resize':
                            if (!isPopupOpened) {
                                iframe.style.height = (data.height + 32) + 'px';
                            }
                            break;
                        case 'open':
                            window.location = data.url;
                            break;
                    }
                }
            }, false);
        }
    </script>

    <!--shrift-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header>
    <div class="container">
        <div class="row justify-content-lg-start justify-content-start">
            <div class="col-sm-3">
                <a href="/" class="logo-lnk text-md-center"><img src="/images/logo.png" alt=""></a>
            </div>

            <div class="col-xl-5 col-sm-9 no-padding d-flex align-items-center justify-content-center justify-content-xl-start">
                <nav class="navbar-expand-sm">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">

                        <div id="hamburger" class="hamburglar is-Closed">

                            <div class="burger-icon">
                                <div class="burger-container">
                                    <span class="burger-bun-top"></span>
                                    <span class="burger-filling"></span>
                                    <span class="burger-bun-bot"></span>
                                </div>
                            </div>

                            <!-- svg ring containter -->
                            <div class="burger-ring">
                                <svg class="svg-ring">
                                    <path class="path" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="4"
                                          d="M 34 2 C 16.3 2 2 16.3 2 34 s 14.3 32 32 32 s 32 -14.3 32 -32 S 51.7 2 34 2"/>
                                </svg>
                            </div>
                            <!-- the masked path that animates the fill to the ring -->

                            <svg width="0" height="0">
                                <mask id="mask">
                                    <path xmlns="http://www.w3.org/2000/svg" fill="none" stroke="#ff0000"
                                          stroke-miterlimit="10" stroke-width="4"
                                          d="M 34 2 c 11.6 0 21.8 6.2 27.4 15.5 c 2.9 4.8 5 16.5 -9.4 16.5 h -4"/>
                                </mask>
                            </svg>
                            <div class="path-burger">
                                <div class="animate-path">
                                    <div class="path-rotation"></div>
                                </div>
                            </div>
                        </div>


                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item <?= Yii::$app->controller->action->id == 'news' ? 'active' : '' ?>">
                                <a class="nav-link"
                                   href="<?= Url::to(['/site/news']) ?>"><?= Yii::t('app', 'Новости') ?></a>
                            </li>
                            <li class="nav-item <?= Yii::$app->controller->action->id == 'album' ? 'active' : '' ?>">
                                <a class="nav-link"
                                   href="<?= Url::to(['/site/album']) ?>"><?= Yii::t('app', 'Фотогалерея') ?></a>
                            </li>
                            <li class="nav-item <?= Yii::$app->controller->action->id == 'about' ? 'active' : '' ?>">
                                <a class="nav-link"
                                   href="<?= Url::to(['/site/about']) ?>"><?= Yii::t('app', 'О компании') ?></a>
                            </li>
                            <li class="nav-item <?= Yii::$app->controller->action->id == 'contacts' ? 'active' : '' ?>" >
                                <a class="nav-link"
                                   href="<?= Url::to(['/site/contacts']) ?>"><?= Yii::t('app', 'Контакты') ?></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <div class="col-xl-4 d-flex align-items-center">
                <div class="hdr-rht-side d-flex justify-content-center justify-content-xl-between w-100">
                    <button type="button" class="bdr-radius pink-btn" data-toggle="modal" data-target="#modal-req">
                        <?= Yii::t('app', 'Отправить заявку') ?>
                    </button>

                    <ul class="d-flex align-items-center head-lang">
                        <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a
                                    href="<?= Url::to(['/site/set-language', 'l' => 'ru']) ?>">Рус</a></li>
                        <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a
                                    href="<?= Url::to(['/site/set-language', 'l' => 'en']) ?>">Eng</a></li>
                        <li class="<?= Yii::$app->language == 'uz' ? 'active' : '' ?>"><a
                                    href="<?= Url::to(['/site/set-language', 'l' => 'uz']) ?>">Uzb</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header><!--end header-->

<?= $content ?>
<?php

$base = new BaseModel();
$feedback = new Feedback();
$callback = new Callback();
?>

<!--********************start footer********************-->

<footer class="footer-page text-center text-md-left">

    <div class="container">
        <div class="row footer-up equal-offsets">
            <div class="col-lg-4 col-md-6 footer-nav-wrap">
                <h4 class="link-col f-medium"><?= Yii::t('app', 'Навигация') ?></h4>
                <ul class="nav-site-rkh">
                    <li><a href="<?= Url::to(['/site/about']) ?>"><?= Yii::t('app', 'О компании') ?></a></li>
                    <li>
                        <a href="<?= Url::to(['/site/page', 'type' => 'corporative']) ?>"><?= Yii::t('app', 'Корпоративным клиентам') ?></a>
                    </li>
                    <li><a href="<?= Url::to(['/site/contacts']) ?>"><?= Yii::t('app', 'Контакты') ?></a></li>
                    <li><a href="<?= Url::to(['/site/faq', 'id' => '1']) ?>"><?= Yii::t('app', 'Помощь') ?></a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-4 f-medium wh-col footer-comp-inf">
                <div class="footer-comp-inf-sect">
                    <div class="footer-logo">
                        <img src="/images/footer-logo.png" alt="">
                    </div>
                    <div class="footer-phone-num">
                        <span><i><img src="/images/phone-icon.png"
                                  alt=""></i> <?= $base->getContacts() ? $base->getContacts()->phone1 : '' ?></span>
                     </div>

                     <div class="footer-adrs">
                         <span><i><img src="/images/map-icon.png"
                                  alt=""></i> <?= $base->getContacts()->address ? $base->getContacts()->address : '' ?></span>
                    </div>
                    <div class="footer-adrs">
                         <span><i><img src="/images/map-icon.png"
                                       alt=""></i> <?= $base->getContacts()->address2 ? $base->getContacts()->address2 : '' ?></span>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 footer-sotsial-nav-wrap d-flex justify-content-center justify-content-md-end">
            <div class="footer-sot-wrap">
                <h4 class="link-col f-medium"><?= Yii::t('app', 'Мы в соц сетях') ?></h4>
                <ul class="sotsial-nav justify-content-center justify-content-md-start sotsial-nav">
                    <?php if ($base->getSocials()): ?>
                        <?php foreach ($base->getSocials() as $item): ?>
                            <li><a href="<?= $item->url ?>"><i class="fa <?= $item->image ?>"></i></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>


                <ul class="market-apps justify-content-center justify-content-md-start">
                    <?php if ($base->getContacts()): ?>
                        <li><a href="<?= $base->getContacts()->url_playmarket ?>"><img src="/images/google-icon.png"
                                                                                       alt=""></a></li>
                        <li><a href="<?= $base->getContacts()->urlappstore ?>"><img src="/images/ios-icon.png"
                                                                                    alt=""></a></li>
                    <?php endif; ?>
                </ul>
            </div>
            </div>
        </div>

        <div class="footer-down text-center text-lg-left">
            <div class="row equal-offsets">
                <div class="col-lg-6"><span><?= Yii::t('app', 'ООО “Рохат Трэвел” Все права защищены') ?></span></div>
                <div class="col-lg-6 text-lg-right"><span>&copy; <?= Yii::t('app', 'Copyright 2018 - Web developed by') ?>
                        <a
                                href="http://sos.uz">SOS Group</a></span></div>
            </div>
        </div>
    </div>
</footer><!--end footer-->

<!--********************phone modal********************-->
<button type="button" class="phone-modal-btn" data-toggle="modal" data-target="#phone-modal">
    <img src="/images/phone-modal-btn-icon.png" alt="Перезвонить">
</button><!--end phone modal-->


<!--modal - window-->
<div class="modal fade" id="modal-req" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-form-content">

            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel">Отправить заявку</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $feed = ActiveForm::begin([
                    'action' => Url::to(['/site/feedback']),
                    'id' => 'feedbackFrom'
                ]) ?>
                <?= $feed->field($feedback, 'fio')->textInput(['placeholder' => Yii::t('app', 'ФИО')])->label(false) ?>
                <?= $feed->field($feedback, 'company')->textInput(['placeholder' => Yii::t('app', 'Компания')])->label(false) ?>
                <?= $feed->field($feedback, 'email')->textInput(['type' => 'email', 'placeholder' => Yii::t('app', 'Email')])->label(false) ?>
                <?= $feed->field($feedback, 'message')->textarea(['placeholder' => Yii::t('app', 'Сообщение')])->label(false) ?>

                <div class="text-right">
                    <button type="submit" class="bdr-radius form-btn"><?= Yii::t('app', 'Отправить') ?></button>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="phone-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-form-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel"><?= Yii::t('app', 'Бесплатный звонок') ?></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $call = ActiveForm::begin([
                    'id' => 'callbackFrom'
                ]) ?>
                <?= $call->field($callback, 'fio')->textInput(['placeholder' => Yii::t('app', 'ФИО')])->label(false) ?>
                <?= $call->field($callback, 'phone')->textInput(['placeholder' => Yii::t('app','Телефон')])->label(false)  ?>

                <div class="text-right">
                    <button type="submit" class="bdr-radius form-btn"><?= Yii::t('app', 'Перезвонить') ?></button>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>


<!--modal - window-->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
