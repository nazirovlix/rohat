<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Контакты');
?>

<!--********************start headTop********************-->

<section class="head-top-wrap" style="background-image:url('/images/head-top-bg.png')">
    <div class="head-top-wrap-desc text-center wh-col">
        <h2><?= Yii::t('app', 'Контакты') ?></h2>
        <ul class="des-breadcrumb d-flex">
            <li><a href="/"><?= Yii::t('app', 'Главная') ?></a></li>
            <li><?= Yii::t('app', 'Контакты') ?></li>
        </ul>
    </div>
</section><!--end headTop-->
<!--********************start Contact********************-->
<div class="bg-white">
<main class="container regular-padding">
    <div class="row equal-offsets">
        <div class="col-md-6">
            <div class="heading">
                <h2 class="f-reg"><?= Yii::t('app', 'Контактные данные') ?></h2>
            </div>
            <div class="contact-data">
                <p class="contact-data-box d-flex">
                    <span class="contact-data-img">
                        <img src="/images/phone-icon-blue.png" alt="">
                    </span>
                    <span class="contact-data-desc"><?= $contacts->phone1 ?></span>
                </p>

                <p class="contact-data-box d-flex">
                    <span class="contact-data-img"><img src="/images/map-icon-blue.png" alt=""></span>
                    <span class="contact-data-desc"><span><?= $contacts->address1 ?></span>
                    <a href="#" data-toggle="modal" data-target="#scheme1"><?= Yii::t('app','Схема проезда') ?></a></span>
                </p>

                <p class="contact-data-box d-flex">
                    <span class="contact-data-img"><img src="/images/phone-icon-blue.png" alt=""></span>
                    <span class="contact-data-desc"><?= $contacts->phone2 ?></span>
                </p>

                <p class="contact-data-box d-flex">
                    <span class="contact-data-img"><img src="/images/map-icon-blue.png" alt=""></span>
                    <span class="contact-data-desc"><span><?= $contacts->address2 ?></span>
                    <a href="#" data-toggle="modal" data-target="#scheme2"><?= Yii::t('app','Схема проезда') ?></a>
                </p>

                <p class="contact-data-box d-flex"><span class="contact-data-img">
                    <img src="/images/message-icon.png"  alt=""></span>
                    <span class="contact-data-desc"><?= $contacts->email ?></span>
                </p>

                <p class="contact-data-box d-flex"><span class="contact-data-img">
                    <img src="/images/calendar-icon.png" alt=""></span>
                    <span class="contact-data-desc"><span><?= $contacts->work_time ?></span></span>
                </p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="heading">
                <h2 class="f-reg"><?= Yii::t('app','Связаться с нами') ?></h2>
            </div>

            <div class="contact-rkh-wrap">
                <?php $feed = ActiveForm::begin() ?>
                <?= $feed->field($feedback, 'fio')->textInput(['placeholder' => Yii::t('app', 'ФИО')])->label(false) ?>
                <?= $feed->field($feedback, 'company')->textInput(['placeholder' => Yii::t('app', 'Компания')])->label(false) ?>
                <?= $feed->field($feedback, 'email')->textInput(['type' => 'email', 'placeholder' => Yii::t('app', 'Email')])->label(false) ?>
                <?= $feed->field($feedback, 'message')->textarea(['placeholder' => Yii::t('app', 'Сообщение')])->label(false) ?>

                <div class="text-right">
                    <button type="submit" class="bdr-radius contact-frm-btn form-btn"><?= Yii::t('app', 'Отправить') ?></button>
                </div>
                <?php ActiveForm::end() ?>
                <input type="hidden" value="<?= Yii::t('app', 'Спасибо!') ?>" id="feedSwal">
                <input type="hidden" value="<?= Yii::t('app', 'Ваше сообщение отправлено!') ?>" id="feedSwal2">
                <?php if (Yii::$app->session->hasFlash('feedback')): ?>
                    <?php $this->registerJs('
                                var swal_a = $("#feedSwal").val();
                                var swal_b = $("#feedSwal2").val();
                                swal(swal_a, swal_b, "success");
                                '); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="modal fade" id="scheme1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-form-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel"><?= Yii::t('app', 'Схема проезда') ?></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="scheme">
                        <img src="/uploads/<?=$contacts->image_scheme1 ?>" class="scheme-img">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="scheme2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-form-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel"><?= Yii::t('app', 'Схема проезда') ?></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="scheme">
                        <img src="/uploads/<?=$contacts->image_scheme2 ?>" class="scheme-img">
                    </div>
                </div>
            </div>
        </div>
    </div>


</main>
</div><!--end Contact-->

