<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error bg-white">
<div class="container regular-padding error-page-main">
    <div class="error-page-img">
        <img src="/images/404.jpg" alt="404-img" />
    </div>

    <a class="back-to-btn" href="/"><i class="fa fa-arrow-left"></i> Вернуться на главную</a>
</div>
</div>
