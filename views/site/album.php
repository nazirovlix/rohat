<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 19.12.2018
 * Time: 17:43
 */

use yii\helpers\Url;

$this->title = Yii::t('app', 'Фотогалерея')
?>

<section class="head-top-wrap" style="background-image:url('/images/head-top-bg.png')">
    <div class="head-top-wrap-desc text-center wh-col">
        <h2><?= Yii::t('app', 'Фотогалерея') ?></h2>
        <ul class="des-breadcrumb d-flex">
            <li><a href="/"><?= Yii::t('app', 'Главная') ?></a></li>
            <li><?= Yii::t('app', 'Фотогалерея') ?></li>
        </ul>
    </div>
</section><!--end headTop-->

<div class="bg-white">
<main class="regular-padding container">
    <div class="row equal-offsets">
        <?php if ($album): ?>
            <?php foreach ($album as $item): ?>
                <div class="col-lg-4 col-md-6">
                    <figure class="gallery-box">
                        <img src="/uploads/<?= $item->image ?>" alt="<?= $item->title ?>">
                        <figcaption class="wh-col">
                            <h4 class="f-medium"><?= $item->title ?></h4>
                            <span class="gallery-box-date"><?= date('d.m.Y', $item->created_at) ?></span>
                            <a href="<?= Url::to(['/site/album-images','id' => $item->id]) ?>" class="gallery-box-lnk-btn"><?= Yii::t('app','Просмотр') ?></a>
                        </figcaption>
                    </figure>
                </div>
            <?php endforeach ?>
        <?php endif; ?>
    </div>

    <?php
    echo \yii\widgets\LinkPager::widget([
        'pagination' => $pages,
        'options' => ['class' => 'des-pagination d-flex justify-content-center flex-wrap'],
        'prevPageLabel' => '&nbsp;',
        'nextPageLabel' => '&nbsp;',
        'prevPageCssClass' => 'des-pag-arrow left-arrow',
        'nextPageCssClass' => 'des-pag-arrow right-arrow',
        'activePageCssClass' => 'active',

    ]);?>
</main><!--end gallery-->
</div>