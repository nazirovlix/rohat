<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 19.12.2018
 * Time: 18:02
 */
$this->title = $offer->airlines;
?>
<div class="main-slider-wrap-con">
<section class="main-slider-wrap">
     <?php if (!empty($offer)): ?>
            <div class="main-slider-elem" style="background-image: url('/uploads/<?= $offer->image ?>')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 main-slider-desc-wrap">
                            <p class="f-24"><?= $offer->flight ? $offer->flight : '' ?></p>
                             <p class="txt-arrow"><?= $offer->transfer ? $offer->transfer : '' ?> &nbsp;</p>
                            <p class="f-24 f-bold">
                               <?= Yii::t('app', 'от <span class="f-48">{price}</span> руб.', ['price' => $offer->price]) ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
 <?php endif; ?>
</section><!--end main slider-->
</div>

<div class="ifarme-airline">
<iframe src="https://superkassa.ru/next/embedded/defaultExtra?partner=b9d1e78cdc8038ec50e2651e16c9329b" style="width: 100%; overflow: auto; margin: 0px; padding: 0px; border: medium none; background: transparent none repeat scroll 0% 0%; height: 196.367px;" onload="superkassaInit(this)" frameborder="0"></iframe>
</div>

<!--********************start payment system wrap********************-->
<div class="bg-white">
<div class="container ">
    <div class=" bordered regular-padding-2" >
        <div class="paym-sys-wrap">
            <?php if (!empty($partners)): ?>
                <?php foreach ($partners as $item): ?>
                    <a href="<?= $item->url ?>">
                        <img src="/uploads/<?= $item->image ?>" alt="<?= $item->name ?>">
                    </a>
                <?php endforeach ?>
            <?php endif; ?>
        </div>
    </div>
</div></div><!--end payment system wrap-->
