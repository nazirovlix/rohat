<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Главная страница');
?>

<!--********************start main slider********************-->

<div class="main-slider-wrap-con">
<section class="main-slider-wrap">
    <?php if (!empty($carousel)): ?>
        <?php foreach ($carousel as $item): ?>
            <div class="main-slider-elem" style="background-image: url('/uploads/<?= $item->image ?>')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 main-slider-desc-wrap">
                            <p class="f-24"><?= $item->flight ? $item->flight : '' ?></p>
                             <p class="txt-arrow"><?= $item->transfer ? $item->transfer : '' ?> &nbsp;</p>
                            <p class="f-24 f-bold">
                                <?= Yii::t('app', 'от <span class="f-48">{price}</span> руб.', ['price' => $item->price]) ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</section><!--end main slider-->
</div>

<div class="ifarme-airline">
    <div id="root"></div>
<!--<iframe src="https://superkassa.ru/next/embedded/defaultExtra?partner=b9d1e78cdc8038ec50e2651e16c9329b" style="width: 100%; overflow: auto; margin: 0px; padding: 0px; border: medium none; background: transparent none repeat scroll 0% 0%; height: 196.367px;" onload="superkassaInit(this)" frameborder="0"></iframe>-->
</div>
<!--********************main about us********************-->

<div class="container-fluid">
    <div class="row">
        <?php if (!empty($about)): ?>
            <div class="col-md-6 no-padding main-about-img-wrap">
                <img src="/uploads/<?= $about->image1 ?>" alt="">
            </div>
            <div class="col-md-6 fx-c no-padding main-about-txt-desc-wrap">
                <div class="main-about-txt-desc wh-col">
                    <p><?= $about->about ? $about->about : '' ?></p>
                    <a href="<?= Url::to(['/site/about']) ?>"
                       class="bdr-radius full-pink-btn bl-xs-center"><?= Yii::t('app', 'Подробнее') ?></a>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div><!--end about us-->


<!--********************start special offers block********************-->

<div class="main-spec-wrap regular-padding bg-white">
    <div class="container">
        <div class="heading">
            <h2><?= Yii::t('app', 'Специальные предложения') ?></h2>
        </div>

        <div class="row spec-offers-slider-wrap">
            <?php if ($offers): ?>
                <?php foreach ($offers as $item): ?>
                    <a href="<?= Url::to(['/site/offers','id' => $item->id]) ?>" class="col-md-4">
                        <article class="spec-offers-elem wh-col"
                                 style="background-image: url('/uploads/<?= $item->image ?>')">
                            <?php if (!empty($item->discount)): ?>
                                <span class="spec-offers-elem-trgle">-<?= $item->discount ?>%</span>
                            <?php endif; ?>

                            <div class="spec-offers-elem-txt-desc">
                                <div>
                                    <h4><?= $item->airlines ?></h4>
                                    <p><?= $item->flight ?></p>
                                    <?php if(!empty($item->price)): ?>
                                    <p><?= Yii::t('app', ' от {price} руб.', ['price' => $item->price]) ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </article>
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div><!--end special offers block-->


<!--********************start special offers block********************-->

<section class="main-contact-wrap regular-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 wh-col mb-lg-15 main-contact-sec-desc">
                <h3><?= Yii::t('app', 'Хочешь узнавать об акциях первым?') ?></h3>
                <p><?= Yii::t('app', 'Подпишись на рассылку!') ?></p>
            </div>
            <div class="col-lg-9">
                <?php $form = ActiveForm::begin() ?>
                <div class="main-contact-sec">
                    <div class="main-contact-sec-item f-40 mb-md-15">
                        <?= $form->field($subscribe, 'name')->textInput(['placeholder' => Yii::t('app', 'Имя')])->label(false) ?>
                    </div>
                    <div class="main-contact-sec-item f-60 w-but">
                        <?= $form->field($subscribe, 'email')->textInput(['type' => 'email', 'placeholder' => Yii::t('app', 'email')])->label(false) ?>
                        <button type="submit" class="bdr-radius subs-btn">Подписаться</button>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
                <input type="hidden" value="<?= Yii::t('app', 'Спасибо за подписку!') ?>" id="followSwal">
                <?php if (Yii::$app->session->hasFlash('follow')): ?>
                    <?php $this->registerJs('
                                var swal_a = $("#followSwal").val();
                                swal(swal_a, "", "success");
                                '); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!--end special offers block-->


<!--********************start main news********************-->
<div class="bg-white">
<div class="container regular-padding">
    <div class="heading text-center text-sm-left  d-flex justify-content-sm-between flex-column flex-sm-row">
        <h2><?= Yii::t('app', 'Новости компании') ?></h2>
        <a href="<?= Url::to(['/site/news']) ?>"
           class="bdr-radius all-news-btn pink-btn"><?= Yii::t('app', 'Все новости') ?></a>
    </div>

    <div class="row equal-offsets">
        <?php if (!empty($news)): ?>
            <?php foreach ($news as $item): ?>
                <div class="col-lg-4">
                    <article class="news-box">
                        <div class="news-box-img">
                            <img src="/uploads/<?= !empty($item->image_little) ? $item->image_little : $item->image_main ?>"
                                 alt="">
                        </div>

                        <div class="news-box-desc">
                            <div class="news-box-desc-date"><?= date('d.m.Y', $item->created_at) ?></div>
                            <a href="<?= Url::to(['/site/news-page','id' => $item->id]) ?>" class="link"><h4><?= $item->title ?></h4></a>
                            <p><?= $item->content ?></p>
                        </div>
                    </article>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
</div><!--end main news-->


<!--********************start payment system wrap********************-->
<div class="bg-white">
<div class="container ">
    <div class=" bordered regular-padding-2" style="border-top:1px solid #adadad">
        <div class="paym-sys-wrap">
            <?php if (!empty($partners)): ?>
                <?php foreach ($partners as $item): ?>
                    <a href="<?= $item->url ?>">
                        <img src="/uploads/<?= $item->image ?>" alt="<?= $item->name ?>">
                    </a>
                <?php endforeach ?>
            <?php endif; ?>
        </div>
    </div>
</div>
</div><!--end payment system wrap-->

