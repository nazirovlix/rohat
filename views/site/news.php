<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 19.12.2018
 * Time: 16:50
 */

use yii\helpers\Url;

$this->title = Yii::t('app', 'Новости')
?>

<!--********************start headTop********************-->

<section class="head-top-wrap" style="background-image:url('/images/head-top-bg.png')">
    <div class="head-top-wrap-desc text-center wh-col">
        <h2><?= Yii::t('app', 'Новости') ?></h2>
        <ul class="des-breadcrumb d-flex">
            <li><a href="/"><?= Yii::t('app', 'Главная') ?></a></li>
            <li><?= Yii::t('app', 'Новости') ?></li>
        </ul>
    </div>
</section><!--end headTop-->


<!--********************start news section********************-->
<div class="bg-white">
<main class="regular-padding container">
    <div class="row equal-offsets">
        <?php if (!empty($news)): ?>
            <?php foreach ($news as $item): ?>
                <div class="col-lg-4">
                    <article class="news-box">
                        <div class="news-box-img">
                            <img src="/uploads/<?= $item->image_little ?>" alt="<?= $item->title ?>">
                        </div>

                        <div class="news-box-desc">
                            <div class="news-box-desc-date"><?= date('d.m.Y', $item->created_at)?></div>
                            <a href="<?= Url::to(['/site/news-page','id' => $item->id]) ?>" class="link"><h4><?= $item->title ?></h4></a>
                            <p><?= mb_substr(strip_tags($item->content), 0,300) ?></p>
                        </div>
                    </article>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <?php
    echo \yii\widgets\LinkPager::widget([
        'pagination' => $pages,
        'options' => ['class' => 'des-pagination d-flex justify-content-center flex-wrap'],
        'prevPageLabel' => '&nbsp;',
        'nextPageLabel' => '&nbsp;',
        'prevPageCssClass' => 'des-pag-arrow left-arrow',
        'nextPageCssClass' => 'des-pag-arrow right-arrow',
        'activePageCssClass' => 'active',

    ]);?>
</main>
</div>

<!--end news section-->

