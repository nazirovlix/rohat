<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 19.12.2018
 * Time: 18:28
 */


    $this->title =  $page->title;

?>

<!--********************start headTop********************-->

<section class="head-top-wrap" style="background-image:url('/images/head-top-bg.png')">
    <div class="head-top-wrap-desc text-center wh-col">
        <h2><?= $page->title ?></h2>
        <ul class="des-breadcrumb d-flex">
            <li><a href="/"><?= Yii::t('app', 'Главная') ?></a></li>
            <li><?= $page->title ?></li>
        </ul>
    </div>
</section><!--end headTop-->

<!--********************start index-client page********************-->
<div class="bg-white">
<div class="container regular-padding">
    <div class="row">
        <div class="col-12 index-client-desc-text">
            <span><?= $page->content ?></span>
        </div>
    </div>
</div><!--end index-client page-->
</div>
<!--********************start section rokhat advanced ********************-->

<section class="rkh-advan-con regular-padding">
    <div class="container">
        <div class="heading">
            <h2 class="f-reg"><?= Yii::t('app','Наши преимущества') ?></h2>
        </div>

        <div class="row equal-offsets">

            <?php if (!empty($advantages)) {
                foreach ($advantages as $item):?>
                <div class="col-lg-3 col-md-6 col-xs-6">
                    <div class="rkh-advan-box">
                        <img src="/uploads/<?= $item->image ?>" alt="">
                        <h4><?= $item->title ?></h4>
                    </div>
                </div>
                <?php endforeach;
            } ?>

        </div>
    </div>
</section><!--end section rokhat advanced-->

<!--********************start our client cont********************-->
<div class="bg-white">
<section class="our-client container  regular-padding">
    <div class="heading">
        <h2 class="f-reg"><?= Yii::t('app','Наши клиенты') ?></h2>
    </div>

    <div class="our-client-slider">
        <?php if (!empty($partners)) {
            foreach ($partners as $item):?>
            <a href="<?= $item->url ?>"><img src="/uploads/<?= $item->image ?>" alt="<?= $item->name ?>"></a>
            <?php endforeach;
        } ?>
    </div>
</section>
</div>
<!--end our clients cont-->
