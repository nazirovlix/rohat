<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app','О компании');
?>
<section class="head-top-wrap" style="background-image:url('/images/head-top-bg.png')">
    <div class="head-top-wrap-desc text-center wh-col">
        <h2><?= Yii::t('app','О компании') ?></h2>
        <ul class="des-breadcrumb d-flex">
            <li><a href="/"><?= Yii::t('app','Главная') ?></a></li>
            <li><?= Yii::t('app','О компании') ?></li>
        </ul>
    </div>
</section><!--end headTop-->
<!--********************start About us********************-->
<div class="bg-white">
<main class="regular-padding container">
    <?php if(!empty($about)):?>
    <div class="row about-us-box-wrap">

        <div class="col-md-6 about-us-box-wrap-img order-md-0">
            <img src="/uploads/<?= $about->image1 ?>" alt="<?= Yii::t('app','О нас') ?>">
        </div>
        <div class="col-md-6 fx-c order-md-1">
            <div class="about-us-box-des">
                <p><?= $about->about ?></p>
            </div>
        </div>

        <div class="col-md-6 fx-c order-md-2 order-3">
            <div class="about-us-box-des">
                <p><?= $about->why ?></p>
            </div>
        </div>
        <div class="col-md-6 about-us-box-wrap-img order-md-3 order-2">
            <img src="/uploads/<?= $about->image3 ?>" alt="">
        </div>
        <div class="col-md-6 about-us-box-wrap-img order-md-4">
            <img src="/uploads/<?= $about->image2 ?>" alt="">
        </div>
        <div class="col-md-6 fx-c order-md-5">
            <div class="about-us-box-des">
                <p><?= $about->mission ?></p>
            </div>
        </div>
    </div>
    <?php endif; ?>
</main>
</div><!--end about us-->

<!--********************start armor box********************-->

<section class="armor-box-wrap" style="background-image: url('/images/armor-box-bg.png')">
    <div class="armor-box-wrap-desc container wh-col">
        <h2 class="f-medium w-lng-arrow"><?= Yii::t('app','Бронируйте билеты прямо сейчас') ?></h2>
        <a href="/" class="booking-tick"><span class="booking-tick-txt"><?= Yii::t('app','Бронировать') ?></span><span class="circle-booking"><i class="fa fa-arrow-right"></i></span></a>
    </div>
</section><!--end armor box-->


<!--********************start payment system wrap********************-->
<div class="bg-white">
<div class="container ">
    <div class=" bordered regular-padding-2">
        <div class="paym-sys-wrap">
            <?php if (!empty($partners)): ?>
                <?php foreach ($partners as $item): ?>
                    <a href="<?= $item->url ?>">
                        <img src="/uploads/<?= $item->image ?>" alt="<?= $item->name ?>">
                    </a>
                <?php endforeach ?>
            <?php endif; ?>
        </div>
    </div>
    </div>
</div><!--end payment system wrap-->