<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 19.12.2018
 * Time: 17:59
 */
$this->title = $album->title
?>


<section class="head-top-wrap" style="background-image:url('/images/head-top-bg.png')">
    <div class="head-top-wrap-desc text-center wh-col">
        <h2><?= Yii::t('app', 'Фотогалерея') ?></h2>
        <ul class="des-breadcrumb d-flex">
            <li><a href="/"><?= Yii::t('app', 'Главная') ?></a></li>
            <li><?= Yii::t('app', 'Фотогалерея') ?></li>
        </ul>
    </div>
</section><!--end headTop-->

<!--********************start gallery inside page********************-->
<div class="bg-white">
<main class="container regular-padding">
    <div class="row gallery-inside-wrap">
        <div class="col-12">
            <div class="gallery-inside-head">
                <h2 class="gallery-inside-head-ttl">Альбом: <?= $album->title ?></h2>
                <span class="gallery-inside-head-date"><?= date('d.m.Y', $album->created_at) ?></span>
            </div>

            <div class="gallery-inside-desc">
                <p><?= strip_tags($album->content) ?></p>
            </div>
        </div>
    </div>

    <div class="row gallery-slider-wrap">
        <div class="col-lg-10">
            <div class="gallery-album-full-slider">
                <?php if($album->albumImages):?>
                    <?php foreach ($album->albumImages as $item):?>
                        <a href="/uploads/<?= $item->img ?>" data-fancybox="gallery" class="fancybox-opened">
                            <img src="/uploads/<?= $item->img ?>" alt="">
                        </a>
                        <?php endforeach;?>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-xl-2 col-lg-2">
            <div class="gallery-album-min-slider">
                <?php if($album->albumImages):?>
                    <?php foreach ($album->albumImages as $item):?>
                    <div>
                        <a class="album-min-slider-item">
                            <img src="/uploads/<?= $item->img ?>" alt="">
                        </a>
                        </div>
                    <?php endforeach;?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <a href="<?= \yii\helpers\Url::to(['/site/album']) ?>" class="prev-link-galler-inside"><i class="fa fa-long-arrow-left"> </i> <?= Yii::t('app','Назад к альбомам') ?></a>
</main>
</div><!--end gallery inside page-->