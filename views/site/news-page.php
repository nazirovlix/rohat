<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 19.12.2018
 * Time: 17:05
 */

use yii\helpers\Url;

if ($news) {
    $this->title = $news->title;
} else {
    $this->title = 'Новости';
}
?>

<!--********************start headTop********************-->

<section class="head-top-wrap" style="background-image:url('/images/head-top-bg.png')">
    <div class="head-top-wrap-desc text-center wh-col">
        <h2><?= $this->title ?></h2>
        <ul class="des-breadcrumb d-flex">
            <li><a href="/"><?= Yii::t('app', 'Главная') ?></a></li>
            <li><a href="<?= Url::to(['/site/news']) ?>"><?= Yii::t('app', 'Новости') ?></a></li>
            <li><?= $this->title ?></li>
        </ul>
    </div>
</section><!--end headTop-->

<!--********************start news-inside********************-->
<div class="bg-white">
<main class="regular-padding container">
    <div class="row equal-offsets">
        <?php if ($news): ?>
            <div class="col-lg-8">
                <div class="news-inside-img">
                    <img src="/uploads/<?= $news->image_main ?>" alt="<?= $news->title ?>">
                </div>

                <div class="news-inside-desc" style="text-indent: 15px;">
                    <p> <?= $news->content ?></p>
                </div>

                <div class="news-inside-lnk">
                    <a href="<?= Url::to(['/site/news']) ?>" class="link-col"><i
                                class="fa fa-long-arrow-left"> </i> <?= Yii::t('app', 'Вернуться к списку новостей') ?>
                    </a>
                </div>
            </div>
        <?php endif; ?>

        <div class="col-lg-4">
            <?php if ($lastNews): ?>
                <div class="news-inside-sidebar">
                    <h3 class="f-medium news-inside-sidebar-ttl"><?= Yii::t('app', 'Последние новости') ?></h3>
                    <?php foreach ($lastNews as $item): ?>
                        <div class="news-box-inside-sidebar">
                            <div class="news-box-inside-sidebar-img">
                                <img src="/uploads/<?= $item->image_little ?>" alt="<?= $item->title ?>">
                            </div>

                            <div class="news-box-inside-sidebar-desc">
                                <span class="news-box-inside-sidebar-desc-date"><?= date('d.m.Y', $item->created_at) ?></span>
                                <a href="<?= Url::to(['/site/news-page', 'id' => $item->id]) ?>">
                                    <h4><?= $item->title ?></h4></a>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            <?php endif; ?>
        </div>
    </div>
</main>
</div>

<!--end news-inside-->


