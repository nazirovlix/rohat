<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 20.12.2018
 * Time: 15:38
 */

use yii\helpers\Url;

$this->title =  Yii::t('app','Часто задаваемые вопросы');
?>
<section class="head-top-wrap" style="background-image:url('/images/head-top-bg.png')">
    <div class="head-top-wrap-desc text-center wh-col">
        <h2><?= Yii::t('app','Помощь') ?></h2>
        <ul class="des-breadcrumb d-flex">
            <li><a href="/"><?= Yii::t('app', 'Главная') ?></a></li>
            <li><?= Yii::t('app','Помощь') ?></li>
        </ul>
    </div>
</section><!--end headTop-->
<div class="bg-white">
<main class="regular-padding container">
    <div class="heading">
        <h2 class="f-reg"><?= Yii::t('app','Часто задаваемые вопросы'); ?></h2>
    </div>

    <div class="row equal-offsets">
        <div class="col-md-4">
            <ul class="faq-cat-lst">
                <?php foreach ($faqs as $item):?>
                <li class="<?= $id == $item->id ? 'active' : '' ?>"><a href="<?= Url::to(['/site/faq','id' => $item->id]) ?>"><?= $item->name ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="col-md-8">
            <?php foreach ($faq as $item):?>
            <div class="faq-ques-box">
                <div class="faq-collapse-head">
                    <p class="faq-collapse-btn" data="sad">
                        <i><img src="/images/faq-icon.png" alt=""></i>
                        <span><?= $item->question ?></span>
                        <i class="fa fa-angle-right faq-arrow"></i>
                    </p>
                </div>
                <div class="faq-ques-txt txt-editor">
                    <span><?= $item->answer ?></span>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</main><!--end index FAQ-->
</div>
