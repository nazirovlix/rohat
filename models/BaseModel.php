<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 *
 * @property mixed $name
 * @property mixed $description
 * @property mixed $title
 * @property mixed $content
 */
class BaseModel extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const FILE_UPLOAD_URL = ['/admin/file-storage/upload'];
    const MAX_FILE_UPLOAD_SIZE = 10000000;
    const MAX_UPLOAD_FILE = 16;

    public $base_file;
    public $base_files;


    /**
     * @return array
     */
    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ]
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['base_file', 'base_files'], 'safe'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'base_file' => Yii::t('app', 'Photo'),
            'base_files' => Yii::t('app', 'Photos'),
        ];
    }

    public function getContacts()
    {
        $contacts = Contacts::findOne(1);
        if($contacts){
            return $contacts;
        }
        return false;
    }

    public function getSocials()
    {
        $socials = Socials::find()
            ->where(['status' => 1])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        if($socials)
            return $socials;

        return false;
    }

    public function getCarousel()
    {
        $carousel = Carousel::find()
            ->where(['status' => 1])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        return $carousel;

    }


}