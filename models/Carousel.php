<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "carousel".
 *
 * @property int $id
 * @property string $image
 * @property string $flight_ru
 * @property string $flight_uz
 * @property string $flight_en
 * @property string $transfer_ru
 * @property string $transfer_uz
 * @property string $transfer_en
 * @property int $price
 * @property int $order
 * @property int $status
 */
class Carousel extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carousel';
    }

    public $base_file;

    /**
     * @return array
     */
    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'order', 'status'], 'integer'],
            [['image', 'flight_ru', 'flight_uz', 'flight_en', 'transfer_ru', 'transfer_uz', 'transfer_en'], 'string', 'max' => 255],
            [['base_file'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'base_file' => Yii::t('app', 'Image'),
            'flight_ru' => Yii::t('app', 'Flight Ru'),
            'flight_uz' => Yii::t('app', 'Flight Uz'),
            'flight_en' => Yii::t('app', 'Flight En'),
            'transfer_ru' => Yii::t('app', 'Transfer Ru'),
            'transfer_uz' => Yii::t('app', 'Transfer Uz'),
            'transfer_en' => Yii::t('app', 'Transfer En'),
            'price' => Yii::t('app', 'Price'),
            'order' => Yii::t('app', 'Order'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getFlight()
    {
        return $this->{'flight_'. Yii::$app->language};
    }

    public function getTransfer()
    {
        return $this->{'transfer_'. Yii::$app->language};
    }
}
