<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "socials".
 *
 * @property int $id
 * @property string $url
 * @property string $image
 * @property int $order
 * @property int $status
 */
class Socials extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'socials';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order', 'status'], 'integer'],
            [['url', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'image' => Yii::t('app', 'Icon'),
            'base_file' => Yii::t('app', 'Image'),
            'order' => Yii::t('app', 'Order'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
