<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "about_us".
 *
 * @property int $id
 * @property string $image1
 * @property string $about_us_ru
 * @property string $about_us_uz
 * @property string $about_us_en
 * @property string $image2
 * @property string $our_mission_ru
 * @property string $our_mission_uz
 * @property string $our_mission_en
 * @property string $image3
 * @property string $why_us_ru
 * @property string $why_us_uz
 * @property string $why_us_en
 */
class AboutUs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_us';
    }

    public $base_file1;
    public $base_file2;
    public $base_file3;

    /**
     * @return array
     */
    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file1',
                'pathAttribute' => 'image1',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file2',
                'pathAttribute' => 'image2',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file3',
                'pathAttribute' => 'image3',
                'baseUrlAttribute' => false
            ]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['about_us_ru', 'about_us_uz', 'about_us_en', 'our_mission_ru', 'our_mission_uz', 'our_mission_en', 'why_us_ru', 'why_us_uz', 'why_us_en'], 'string'],
            [['image1', 'image2', 'image3'], 'string', 'max' => 255],
            [['base_file1','base_file2','base_file3'], 'safe']
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image1' => Yii::t('app', 'Image1'),
            'base_file1' => Yii::t('app', 'Image for About US'),
            'base_file2' => Yii::t('app', 'Image for Our mission'),
            'base_file3' => Yii::t('app', 'Image for Why us' ),
            'about_us_ru' => Yii::t('app', 'About Us Ru'),
            'about_us_uz' => Yii::t('app', 'About Us Uz'),
            'about_us_en' => Yii::t('app', 'About Us En'),
            'image2' => Yii::t('app', 'Image2'),
            'our_mission_ru' => Yii::t('app', 'Our Mission Ru'),
            'our_mission_uz' => Yii::t('app', 'Our Mission Uz'),
            'our_mission_en' => Yii::t('app', 'Our Mission En'),
            'image3' => Yii::t('app', 'Image3'),
            'why_us_ru' => Yii::t('app', 'Why Us Ru'),
            'why_us_uz' => Yii::t('app', 'Why Us Uz'),
            'why_us_en' => Yii::t('app', 'Why Us En'),
        ];
    }

    public function getAbout()
    {
        return $this->{'about_us_'.Yii::$app->language};
    }

    public function getMission()
    {
        return $this->{'our_mission_'.Yii::$app->language};
    }
    public function getWhy()
    {
        return $this->{'why_us_'.Yii::$app->language};
    }
}
