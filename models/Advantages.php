<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "advantages".
 *
 * @property int $id
 * @property string $image
 * @property string $title_ru
 * @property string $title_uz
 * @property string $title_en
 * @property int $status
 */
class Advantages extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advantages';
    }
    public $base_file;


    /**
     * @return array
     */
    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ]
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_uz', 'title_en'], 'required'],
            [['status'], 'integer'],
            [['image', 'title_ru', 'title_uz', 'title_en'], 'string', 'max' => 255],
            [['base_file'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'base_file' => Yii::t('app', 'Image'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'title_en' => Yii::t('app', 'Title En'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getTitle()
    {
        return $this->{'title_'.Yii::$app->language};
    }
}
