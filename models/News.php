<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_uz
 * @property string $title_en
 * @property string $content_ru
 * @property string $content_uz
 * @property string $content_en
 * @property string $image_main
 * @property string $image_little
 * @property string $slug
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class News extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }
    public $base_file;
    public $base_file2;


    /**
     * @return array
     */
    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file',
                'pathAttribute' => 'image_main',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file2',
                'pathAttribute' => 'image_little',
                'baseUrlAttribute' => false
            ],
            [
                'class' => TimestampBehavior::className()
            ]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_uz', 'title_en'], 'required'],
            [['content_ru', 'content_uz', 'content_en'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['title_ru', 'title_uz', 'title_en', 'image_main', 'image_little', 'slug'], 'string', 'max' => 255],
            [['base_file','base_file2'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'base_file' => Yii::t('app', 'Main image'),
            'base_file2' => Yii::t('app', 'Little image'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'title_en' => Yii::t('app', 'Title En'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_uz' => Yii::t('app', 'Content Uz'),
            'content_en' => Yii::t('app', 'Content En'),
            'image_main' => Yii::t('app', 'Image Main'),
            'image_little' => Yii::t('app', 'Image Little'),
            'slug' => Yii::t('app', 'Slug'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    public function getTitle()
    {
        return $this->{'title_'.Yii::$app->language};
    }

    public function getContent()
    {
        return $this->{'content_'.Yii::$app->language};
    }
}
