<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "offers".
 *
 * @property int $id
 * @property string $image
 * @property string $airlines_ru
 * @property string $airlines_uz
 * @property string $airlines_en
 * @property string $flight_ru
 * @property string $flight_uz
 * @property string $flight_en
 * @property string $transfer_ru
 * @property string $transfer_uz
 * @property string $transfer_en
 * @property int $price
 * @property int $discount
 * @property string $slug
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Offers extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers';
    }

    public $base_file;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className()

            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['airlines_ru', 'airlines_uz', 'airlines_en'], 'required'],
            [['price', 'discount', 'status', 'created_at', 'updated_at'], 'integer'],
            [['image', 'airlines_ru', 'airlines_uz', 'airlines_en', 'flight_ru', 'flight_uz', 'flight_en', 'transfer_ru', 'transfer_uz', 'transfer_en', 'slug'], 'string', 'max' => 255],
            [['base_file'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'base_file' => Yii::t('app', 'Image'),
            'airlines_ru' => Yii::t('app', 'Airlines Ru'),
            'airlines_uz' => Yii::t('app', 'Airlines Uz'),
            'airlines_en' => Yii::t('app', 'Airlines En'),
            'flight_ru' => Yii::t('app', 'Flight Ru'),
            'flight_uz' => Yii::t('app', 'Flight Uz'),
            'flight_en' => Yii::t('app', 'Flight En'),
            'transfer_ru' => Yii::t('app', 'Transfer Ru'),
            'transfer_uz' => Yii::t('app', 'Transfer Uz'),
            'transfer_en' => Yii::t('app', 'Transfer En'),
            'price' => Yii::t('app', 'Price'),
            'discount' => Yii::t('app', 'Discount'),
            'slug' => Yii::t('app', 'Slug'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getAirlines()
    {
        return $this->{'airlines_'.Yii::$app->language};
    }

    public function getFlight()
    {
        return $this->{'flight_'.Yii::$app->language};
    }
    public function getTransfer()
    {
        return $this->{'transfer_'.Yii::$app->language};
    }
}
