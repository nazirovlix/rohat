<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "album".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_uz
 * @property string $title_en
 * @property string $content_ru
 * @property string $content_uz
 * @property string $content_en
 * @property string $slug
 * @property string $image
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property AlbumImages[] $albumImages
 */
class Album extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'album';
    }
    public $base_file;
    public $base_files;


    /**
     * @return array
     */
    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_files',
                'multiple' => true,
                'uploadRelation' => 'albumImages',
                'pathAttribute' => 'img',
                'orderAttribute' => 'sort',
                'baseUrlAttribute' => false
            ],
            [
                'class' => TimestampBehavior::className()
            ]

        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_uz', 'title_en'], 'required'],
            [['content_ru', 'content_uz', 'content_en'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['title_ru', 'title_uz', 'title_en', 'slug', 'image'], 'string', 'max' => 255],
            [['base_file','base_files'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'title_en' => Yii::t('app', 'Title En'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_uz' => Yii::t('app', 'Content Uz'),
            'content_en' => Yii::t('app', 'Content En'),
            'slug' => Yii::t('app', 'Slug'),
            'image' => Yii::t('app', 'Image'),
            'base_files' => Yii::t('app', 'Album Images'),
            'base_file' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbumImages()
    {
        return $this->hasMany(AlbumImages::className(), ['album_id' => 'id']);
    }
    public function getTitle()
    {
        return $this->{'title_'.Yii::$app->language};
    }

    public function getContent()
    {
        return $this->{'content_'.Yii::$app->language};
    }
}
