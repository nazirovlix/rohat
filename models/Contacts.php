<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $phone1
 * @property string $address1_ru
 * @property string $address1_uz
 * @property string $address1_en
 * @property string $image_scheme1
 * @property string $phone2
 * @property string $address2_ru
 * @property string $address2_uz
 * @property string $address2_en
 * @property string $image_scheme2
 * @property string $email
 * @property string $work_time
 * @property string $url_playmarket
 * @property string $urlappstore
 */
class Contacts extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }
    public $base_file;
    public $base_file1;

    /**
     * @return array
     */
    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file',
                'pathAttribute' => 'image_scheme1',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file1',
                'pathAttribute' => 'image_scheme2',
                'baseUrlAttribute' => false
            ]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone1', 'address1_ru', 'address1_uz', 'address1_en', 'image_scheme1', 'phone2','phone3','phone4', 'address2_ru', 'address2_uz', 'address2_en', 'image_scheme2', 'email', 'work_time', 'url_playmarket', 'urlappstore'], 'string', 'max' => 255],
            [['base_file','base_file1'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'phone1' => Yii::t('app', 'Phone1'),
            'address1_ru' => Yii::t('app', 'Address1 Ru'),
            'address1_uz' => Yii::t('app', 'Address1 Uz'),
            'address1_en' => Yii::t('app', 'Address1 En'),
            'image_scheme1' => Yii::t('app', 'Image Scheme1'),
            'base_file' => Yii::t('app', 'Image Scheme1'),
            'base_file1' => Yii::t('app', 'Image Scheme2'),
            'phone2' => Yii::t('app', 'Phone2'),
            'address2_ru' => Yii::t('app', 'Address2 Ru'),
            'address2_uz' => Yii::t('app', 'Address2 Uz'),
            'address2_en' => Yii::t('app', 'Address2 En'),
            'image_scheme2' => Yii::t('app', 'Image Scheme2'),
            'email' => Yii::t('app', 'Email'),
            'work_time' => Yii::t('app', 'Work Time'),
            'url_playmarket' => Yii::t('app', 'Url Play Market'),
            'urlappstore' => Yii::t('app', 'Url Appstore'),
            'phone3' => Yii::t('app', 'Phone1 Messenger'),
            'phone4' => Yii::t('app', 'Phone2 Messenger'),
        ];
    }

    public function getAddress()
    {
        return $this->{'address1_'.Yii::$app->language};
    }
    public function getAddress1()
    {
        return $this->{'address1_'.Yii::$app->language};
    }
    public function getAddress2()
    {
        return $this->{'address2_'.Yii::$app->language};
    }
}
