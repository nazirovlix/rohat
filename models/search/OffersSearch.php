<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Offers;

/**
 * OffersSearch represents the model behind the search form of `app\models\Offers`.
 */
class OffersSearch extends Offers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'price', 'discount', 'status', 'created_at', 'updated_at'], 'integer'],
            [['image', 'airlines_ru', 'airlines_uz', 'airlines_en', 'flight_ru', 'flight_uz', 'flight_en', 'transfer_ru', 'transfer_uz', 'transfer_en', 'slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Offers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'discount' => $this->discount,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'airlines_ru', $this->airlines_ru])
            ->andFilterWhere(['like', 'airlines_uz', $this->airlines_uz])
            ->andFilterWhere(['like', 'airlines_en', $this->airlines_en])
            ->andFilterWhere(['like', 'flight_ru', $this->flight_ru])
            ->andFilterWhere(['like', 'flight_uz', $this->flight_uz])
            ->andFilterWhere(['like', 'flight_en', $this->flight_en])
            ->andFilterWhere(['like', 'transfer_ru', $this->transfer_ru])
            ->andFilterWhere(['like', 'transfer_uz', $this->transfer_uz])
            ->andFilterWhere(['like', 'transfer_en', $this->transfer_en])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
