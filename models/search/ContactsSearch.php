<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contacts;

/**
 * ContactsSearch represents the model behind the search form of `app\models\Contacts`.
 */
class ContactsSearch extends Contacts
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['phone1', 'address1_ru', 'address1_uz', 'address1_en', 'image_scheme1', 'phone2', 'address2_ru', 'address2_uz', 'address2_en', 'image_scheme2', 'email', 'work_time', 'url_playmarket', 'urlappstore'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contacts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'phone1', $this->phone1])
            ->andFilterWhere(['like', 'address1_ru', $this->address1_ru])
            ->andFilterWhere(['like', 'address1_uz', $this->address1_uz])
            ->andFilterWhere(['like', 'address1_en', $this->address1_en])
            ->andFilterWhere(['like', 'image_scheme1', $this->image_scheme1])
            ->andFilterWhere(['like', 'phone2', $this->phone2])
            ->andFilterWhere(['like', 'address2_ru', $this->address2_ru])
            ->andFilterWhere(['like', 'address2_uz', $this->address2_uz])
            ->andFilterWhere(['like', 'address2_en', $this->address2_en])
            ->andFilterWhere(['like', 'image_scheme2', $this->image_scheme2])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'work_time', $this->work_time])
            ->andFilterWhere(['like', 'url_playmarket', $this->url_playmarket])
            ->andFilterWhere(['like', 'urlappstore', $this->urlappstore]);

        return $dataProvider;
    }
}
