<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faqs".
 *
 * @property int $id
 * @property int $category_id
 * @property string $question_ru
 * @property string $question_uz
 * @property string $question_en
 * @property string $answer_ru
 * @property string $answer_uz
 * @property string $answer_en
 * @property int $status
 * @property int $order
 * @property int $created_at
 * @property int $updated_at
 *
 * @property FaqCategories $category
 */
class Faqs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faqs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['category_id', 'status', 'order', 'created_at', 'updated_at'], 'integer'],
            [['question_ru', 'question_uz', 'question_en'], 'string', 'max' => 255],
            [['answer_ru', 'answer_uz', 'answer_en'],'string'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => FaqCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'question_ru' => Yii::t('app', 'Question Ru'),
            'question_uz' => Yii::t('app', 'Question Uz'),
            'question_en' => Yii::t('app', 'Question En'),
            'answer_ru' => Yii::t('app', 'Answer Ru'),
            'answer_uz' => Yii::t('app', 'Answer Uz'),
            'answer_en' => Yii::t('app', 'Answer En'),
            'status' => Yii::t('app', 'Status'),
            'order' => Yii::t('app', 'Order'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FaqCategories::className(), ['id' => 'category_id']);
    }

    public function getQuestion()
    {
        return $this->{'question_'. Yii::$app->language};
    }
    public function getAnswer()
    {
        return $this->{'answer_'. Yii::$app->language};
    }
}
