<?php

namespace app\controllers;

use app\models\AboutUs;
use app\models\Advantages;
use app\models\Album;
use app\models\BaseModel;
use app\models\Callback;
use app\models\Contacts;
use app\models\FaqCategories;
use app\models\Faqs;
use app\models\Feedback;
use app\models\Follow;
use app\models\News;
use app\models\Offers;
use app\models\Pages;
use app\models\Partners;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class SiteController extends Controller
{

    /**
     * set Language from cookies
     *
     */
    public function init()
    {

        if (!empty(Yii::$app->request->cookies['language'])) {
            Yii::$app->language = Yii::$app->request->cookies['language'];
        } else {
            Yii::$app->language = 'ru';
        }
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $base = new BaseModel();
        $about = AboutUs::findOne(1);
        $offers = Offers::find()->where(['status' => 1])->all();
        $subscribe = new Follow();
        if ($subscribe->load(Yii::$app->getRequest()->post()) && $subscribe->save()) {
            Yii::$app->session->setFlash('follow');
            return $this->refresh();
        }

        $news = News::find()->where(['status' => 1])->orderBy(['created_at' => SORT_DESC])->limit(3)->all();
        $partners = Partners::find()->where(['status' => 1, 'type' => 0])->all();
        $this->layout = 'index_layout';

        return $this->render('index', [
            'carousel' => $base->getCarousel(),
            'about' => $about,
            'offers' => $offers,
            'subscribe' => $subscribe,
            'news' => $news,
            'partners' => $partners

        ]);
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $about = AboutUs::findOne(1);
        $partners = Partners::find()->where(['status' => 1, 'type' => 0])->all();

        return $this->render('about', [
            'about' => $about,
            'partners' => $partners,
        ]);
    }

    /**
     * @return string
     */
    public function actionNews()
    {
        $title = 'title_' . Yii::$app->language;
        $query = News::find()
            ->where(['status' => 1])
            ->andWhere(['not', [$title => '']]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 6]);
        $news = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('news', [
            'news' => $news,
            'pages' => $pages
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionNewsPage($id)
    {
        $news = News::findOne($id);
        $lastNews = News::find()->where(['<>', 'id', $news->id])->orderBy(['id' => SORT_DESC])->limit(5)->all();
        return $this->render('news-page', [
            'news' => $news,
            'lastNews' => $lastNews
        ]);
    }

    /**
     * @return string
     */
    public function actionAlbum()
    {
        $query = Album::find()->where(['status' => 1])->orderBy(['id' => SORT_DESC]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 6]);
        $album = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('album', [
            'album' => $album,
            'pages' => $pages
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionAlbumImages($id)
    {
        $album = Album::findOne($id);

        return $this->render('album-page', [
            'album' => $album
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionOffers($id)
    {
        $offer = Offers::findOne($id);
        $partners = Partners::find()->where(['status' => 1, 'type' => 0])->all();

        return $this->render('offer', [
            'offer' => $offer,
            'partners' => $partners,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContacts()
    {
        $contacts = Contacts::findOne(1);
        $feedback = new Feedback();
        if ($feedback->load(Yii::$app->request->post()) && $feedback->save()) {
            Yii::$app->session->setFlash('feedback');
            return $this->refresh();
        }
        return $this->render('contact', [
            'contacts' => $contacts,
            'feedback' => $feedback,
        ]);
    }

    public function actionFaq($id = null)
    {
        if (!$id) {
            $faq_id = FaqCategories::find()->limit(1)->one();
            $id = $faq_id->id;
        }
        $faqs = FaqCategories::find()->all();
        $faq = Faqs::find()->where(['category_id' => $id, 'status' => 1])->orderBy(['order' => SORT_ASC])->all();

        return $this->render('faq', [
            'faqs' => $faqs,
            'faq' => $faq,
            'id' => $id
        ]);


    }

    /**
     * @param $type
     * @return string
     */
    public function actionPage($type)
    {
        $page = Pages::findOne(['type' => $type]);
        $advantages = Advantages::find()->where(['status' => 1])->all();
        $partners = Partners::find()->where(['type' => '1', 'status' => 1])->all();

        return $this->render('page', [
            'page' => $page,
            'advantages' => $advantages,
            'partners' => $partners

        ]);
    }

    public function actionSetLanguage($l)
    {
        $langs = ['uz', 'ru', 'en'];
        if (in_array($l, $langs)) {
            Yii::$app->language = $l;
            Yii::$app->session->set('app_lang', $l);
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'language',
                'value' => $l,
            ]));
        }
        return $this->redirect(Yii::$app->request->referrer);

    }

    /**
     * Feedback AJAX
     * @return bool
     */
    public function actionFeedback()
    {
        $feed = new Feedback();
        if ($feed->load(Yii::$app->request->post()) && $feed->save()) {
            return true;
        }
        return false;
    }

    /**
     * Callback save AJAX
     * @return bool
     */
    public function actionCallback()
    {
        $feed = new Callback();
        if ($feed->load(Yii::$app->request->post()) && $feed->save()) {
            return true;
        }
        return false;
    }


}
